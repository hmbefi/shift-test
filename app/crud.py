from sqlalchemy.orm import Session
from . import models, schemas
from bcrypt import hashpw, gensalt
from random import choice
from datetime import date


salaries = [s for s in range(30000, 70000, 5000)]
raise_dates = [date(2023, m, 1) for m in range(7, 13)]


def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_username(db: Session, username: str):
    return db.query(models.User).filter(models.User.username == username).first()


def create_user(db: Session, user: schemas.UserCreate):
    db_user = models.User(
        username=user.username,
        hashed_password=hashpw(str.encode(user.password), gensalt()),
        salary=choice(salaries),
        raise_date=choice(raise_dates)
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user
