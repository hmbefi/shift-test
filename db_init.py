from app import database, models
import string
from random import choice, choices
from datetime import date
from bcrypt import hashpw, gensalt
import csv


if __name__ == "__main__":
    salaries = [s for s in range(30000, 70000, 5000)]
    raise_dates = [date(2023, m, 1) for m in range(7, 13)]
    symbols = string.ascii_letters + string.digits

    creds = []
    database.Base.metadata.create_all(bind=database.engine)
    db = database.SessionLocal()
    try:
        for _ in range(10):
            username = ''.join(choices(symbols, k=10))
            password = ''.join(choices(symbols, k=10))
            creds.append([username, password])
            user = models.User(
                username=username,
                hashed_password=hashpw(str.encode(password), gensalt()),
                salary=choice(salaries),
                raise_date=choice(raise_dates)
            )
            db.add(user)
        db.commit()
    finally:
        db.close()

    with open("./app/data/creds.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(["username", "password"])
        writer.writerows(creds)
